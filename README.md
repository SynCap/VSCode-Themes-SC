# SC theme pack for Visual Studio Code

![][logo]

Themes:


- SC Oblivion theme (modified Oblivion color scheme, dark)

	![][1]

- Arctic Night SC (dark)

	![][2]

- Soft 90th SC (light)

	![][3]


- Tropic bird SC (dark)

	![][4]

Modified icon packs

- Colored icons based on [Material icons](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
- Minimal monochrome icons set

![](https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/samples/icons.png)

Source code [home](https://gitlab.com/SynCap/VSCode-Themes-SC/)

[1]: https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/samples/1.png
[2]: https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/samples/2.png
[3]: https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/samples/3.png
[4]: https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/samples/4.png

[logo]: https://gitlab.com/SynCap/VSCode-Themes-SC/raw/master/img/sc-themes-logo.png