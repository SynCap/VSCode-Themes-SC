
## 1.4.1 (2017-07-07)

* patch icon samples ([11099d7](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/11099d7))




# 1.4.0 (2017-07-07)

* New icons sample ([0d94b49](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/0d94b49))




## 1.3.5 (2017-07-07)

* back to Kief's special folders icons ([4ade230](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/4ade230))
* icon sample ([cf387d4](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/cf387d4))
* icons sample new look ([12e4c02](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/12e4c02))
* New folder icons restyle ([1e55132](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/1e55132))




## 1.3.4 (2017-07-06)

* icon sample new look ([3e960c0](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/3e960c0))
* icon sample new look ([5f73457](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/5f73457))
* New icons log ([72e0a2f](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/72e0a2f))
* one more: icons sample new look ([91479ea](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/91479ea))
* SQL file icon added ([95a23aa](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/95a23aa))
* SQL icon ([1db5ea4](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/1db5ea4))
* sql, log, error icons ([590e8ed](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/590e8ed))




## 1.3.2 (2017-07-06)

* fix duplicate key .htaccess patch ([77b61cd](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/77b61cd))
* fix duplicate keys - .htaccess ([3524fac](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/3524fac))




## 1.3.1 (2017-07-06)

* fix duplicate keys ([8b9c7bb](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/8b9c7bb))
* fix duplicate keys patch ([0206462](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/0206462))




# 1.3.0 (2017-07-06)

* compact icon groups ([9934432](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/9934432))
* css family icon styles ([a957cac](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/a957cac))
* fix vis ([6525c23](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/6525c23))
* Icon R+S ([45b4571](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/45b4571))
* Icons - LESS ([8536dfa](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/8536dfa))
* icons A1 ([943b467](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/943b467))
* icons A2 ([88aaef1](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/88aaef1))
* icons B ([90b7afa](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/90b7afa))
* icons C ([a6a218e](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/a6a218e))
* icons D ([810425c](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/810425c))
* Icons E ([f98f038](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/f98f038))
* Icons F ([d997d50](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/d997d50))
* Icons G ([1274f69](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/1274f69))
* Icons H ([f6bc922](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/f6bc922))
* Icons I ([edcf246](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/edcf246))
* Icons J ([66daa00](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/66daa00))
* icons K ([f0aac27](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/f0aac27))
* icons L ([aa729f5](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/aa729f5))
* Icons M ([ee2ad6a](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/ee2ad6a))
* Icons N ([569cbad](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/569cbad))
* Icons O+P ([76f4b5e](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/76f4b5e))
* Icons T ([6f89d16](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/6f89d16))
* Icons U+V ([4d95c1d](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/4d95c1d))
* Icons U+W+X+Y+Z ([ffff3b5](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/ffff3b5))
* manual clean ([2b04689](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/2b04689))




## 1.2.4 (2017-07-05)

* icons demo + apache type + coffee lang icon ([4a8ab88](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/4a8ab88))
* publish 1.2.4 ([aa698d0](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/aa698d0))




## 1.2.3 (2017-07-04)

* apache files ([dfc5ccf](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/dfc5ccf))
* show icons in 32px ([337335d](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/337335d))
* styles to end ([db506a9](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/db506a9))
* v1.2.3 ([549af47](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/549af47))




## 1.2.2 (2017-07-04)

* pathes in MD ([a0bea2f](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/a0bea2f))




## 1.2.1 (2017-07-04)

* July Icons ([33a9e67](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/33a9e67))




# 1.2.0 (2017-07-03)

* 1.2.0 ([5d1d28a](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/5d1d28a))




## 1.1.8 (2017-03-09)

* published 1.1.8 ([9fc9298](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/9fc9298))
* samples cropped a some ([d7f9bd2](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/d7f9bd2))




## 1.1.7 (2017-03-09)

* crop samples, skipt them and svg logos ([dd95f1c](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/dd95f1c))
* publish 1.1.7 ([985626f](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/985626f))




## 1.1.6 (2017-03-09)

* Real McCoy! ([bbc4f5b](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/bbc4f5b))




## 1.1.3 (2017-03-09)

* images, publish ([3f08e2e](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/3f08e2e))
* vsix upgrade ([10c0713](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/10c0713))




# 1.0.0 (2017-03-09)

* Add themes ([ac6067d](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/ac6067d))
* Change themes names ([6c5c509](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/6c5c509))
* fix for my pack ([17b945c](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/17b945c))
* icons, images & publish ([bb39b10](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/bb39b10))
* Initial ([cc01d7c](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/cc01d7c))
* logo ([97f9662](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/97f9662))
* logos & img optimize ([2a3c352](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/2a3c352))
* md icons ([ed13659](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/ed13659))
* New types for existing icons ([49c7612](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/49c7612))
* nodejs icon - dark bg ([f036322](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/f036322))
* old themes names ([26bfffb](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/26bfffb))
* project beautify ([b7f767c](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/b7f767c))
* project beautify ([06fde48](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/06fde48))
* readme images ([9efdcb3](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/9efdcb3))
* readme images => JPEG ([c32ebe5](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/c32ebe5))
* stop JPEG :D 8) ([a7dc8e6](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/a7dc8e6))
* themes rename ([717def8](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/717def8))
* vsicon folder expanded ([24d8704](https://gitlab.com/SynCap/VSCode-Themes-SC/commit/24d8704))



