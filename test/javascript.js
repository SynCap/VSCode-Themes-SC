(function ($) {
    // оболочка для кода подключаемого модуля
    $.fn.highlightOnce2 = function (options) {
        // значения по умолчанию
        $.fn.highlightOnce2.defaults = {
            color: '#fff111',
            duration: 'fast',
            complete: null,
            setup: null
        };    
        options = $.extend($.fn.highlightOnce2.defaults, options);
      
        // код поключаемого модуля
        return this.each(function () {
            var element = $(this);
            // выполнить что либо в отношении каждого элемента          
            
            // прямой вызов callBack  для события setup
            $.isFunction(options.setup) && options.setup.call(this);
        });
 
        
    }
})(jQuery);