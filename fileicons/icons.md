# Material Icons

## Folders

![](material/folder.svg "Folder")
![](material/folder-blue.svg "Blue")
![](material/folder-config.svg "Config")
![](material/folder-css.svg "CSS")
![](material/folder-dist.svg "Dist")
![](material/folder-docs.svg "Docs")
![](material/folder-expo.svg "Expo")
![](material/folder-font.svg "Font")
![](material/folder-git.svg "Git")
![](material/folder-images.svg "Images")
![](material/folder-js.svg "Js")
![](material/folder-less.svg "LESS")
![](material/folder-node.svg "Node")
![](material/folder-sass.svg "SASS")
![](material/folder-scripts.svg "Scripts")
![](material/folder-src.svg "Src")
![](material/folder-stylus.svg "Stylus")
![](material/folder-test.svg "Test")
![](material/folder-views.svg "Views")
![](material/folder-vscode.svg "Vscode")
![](material/folder-vue.svg "Vue")

![](material/folder-open.svg "Blue")
![](material/folder-blue-open.svg "Blue Open")
![](material/folder-config-open.svg "Config")
![](material/folder-css-open.svg "CSS")
![](material/folder-dist-open.svg "Dist")
![](material/folder-docs-open.svg "Docs")
![](material/folder-expo-open.svg "Expo")
![](material/folder-font-open.svg "Font")
![](material/folder-git-open.svg "Git")
![](material/folder-images-open.svg "Images")
![](material/folder-js-open.svg "Js")
![](material/folder-less-open.svg "LESS")
![](material/folder-node-open.svg "Node")
![](material/folder-sass-open.svg "SASS")
![](material/folder-scripts-open.svg "Scripts")
![](material/folder-src-open.svg "Src")
![](material/folder-stylus-open.svg "Stylus")
![](material/folder-test-open.svg "Test")
![](material/folder-views-open.svg "Views")
![](material/folder-vscode-open.svg "Vscode")
![](material/folder-vue-open.svg "Vue")

![](material/folder-outline.svg "Outline")
![](material/folder-outline_.svg "Outline_")
_._


## Files

A –
![](material/actionscript.svg "Actionscript")
![](material/android.svg "Android")
![](material/angular.svg "Angular")
![](material/angular-component.svg "Angular Component")
![](material/angular-directive.svg "Angular Directive")
![](material/angular-guard.svg "Angular Guard")
![](material/angular-pipe.svg "Angular Pipe")
![](material/angular-routing.svg "Angular Routing")
![](material/angular-routing-w.svg "Angular Routing White Arrow")
![](material/angular-service.svg "Angular Service")
![](material/apache.svg "Apache")
![](material/apiblueprint.svg "Apiblueprint")
![](material/applescript.svg "Applescript")
![](material/appveyor.svg "Appveyor")
![](material/arduino.svg "Arduino")
![](material/assembly.svg "Assembly")
![](material/autohotkey.svg "Autohotkey")
![](material/autoit.svg "Autoit")
_._
B –
![](material/babel.svg "Babel")
![](material/bithound.svg "Bithound")
![](material/bower.svg "Bower")
![](material/bucklescript.svg "Bucklescript")

C –
![](material/c-lang.svg "C Lang")
![](material/cake.svg "Cake")
![](material/certificate.svg "Certificate")
![](material/changelog.svg "Changelog")
![](material/clojure.svg "Clojure")
![](material/cmake.svg "Cmake")
![](material/coffee_.svg "Coffee_")
![](material/coffee_sc.svg "Coffee SC")
![](material/conduct.svg "Conduct")
![](material/console.svg "Console")
![](material/console2.svg "Console 2")
![](material/console_sh.svg "Console SH")
![](material/contributing.svg "Contributing")
![](material/cpp.svg "Cpp")
![](material/credits.svg "Credits")
![](material/csharp.svg "Csharp")
![](material/css-map.svg "CSS Map")
![](material/css.svg "CSS")
![](material/cucumber.svg "Cucumber")

D –
![](material/dart.svg "Dart")
![](material/database.svg "Database")
![](material/diff.svg "Diff")
![](material/docker.svg "Docker")
![](material/document.svg "Document")
![](material/document_.svg "Document_")
_._
E –
![](material/editorconfig.svg "Editorconfig")
![](material/elixir.svg "Elixir")
![](material/elm.svg "Elm")
![](material/email.svg "Email")
![](material/erlang.svg "Erlang")
![](material/error.svg "error")
![](material/eslint.svg "Eslint" )
![](material/exe.svg "Exe")
![](material/exe2.svg "Exe2")

F –
![](material/favicon.svg "Favicon")
![](material/file.svg "File")
![](material/file2.svg "File2")
![](material/flash.svg "Flash")
![](material/flow.svg "Flow")
![](material/font_A.svg "Font A")
![](material/font.svg "Font")
![](material/fsharp.svg "Fsharp")
![](material/fusebox.svg "Fusebox")
_._
G –
![](material/git.svg "Git")
![](material/gitlab.svg "Gitlab")
![](material/go.svg "Go")
![](material/gopher.svg "Gopher")
![](material/gradle.svg "Gradle")
![](material/graphql.svg "Graphql")
![](material/groovy.svg "Groovy")
![](material/gulp.svg "Gulp")

H –
![](material/haml.svg "Haml")
![](material/handlebars.svg "Handlebars")
![](material/haskell.svg "Haskell")
![](material/haxe.svg "Haxe" )
![](material/heroku.svg "Heroku")
![](material/html.svg "Html")
_._
I –
![](material/image.svg "Image")
![](material/info-ns.svg "Info-Ns")
![](material/ionic.svg "Ionic")
_._
J –
![](material/java.svg "Java")
![](material/java_.svg "Java_")
![](material/javascript-b-map.svg "Javascript B Map")
![](material/javascript-b.svg "Javascript B")
![](material/javascript-map.svg "Javascript  Map")
![](material/javascript.svg "Javascript")
![](material/json.svg "Json")
![](material/jsx-js.svg "Javascript B2")
![](material/jsx.svg "Jsx")
![](material/julia.svg "Julia")

K –
![](material/karma.svg "Karma")
![](material/key.svg "Key")
![](material/kl.svg "Kl")
![](material/kotlin.svg "Kotlin")
_._
L –
![](material/laravel.svg "Laravel")
![](material/less.svg "LESS")
![](material/less-2.svg "LESS")
![](material/lib.svg "Lib")
![](material/livescript.svg "Livescript")
![](material/lock.svg "Lock")
![](material/log.svg "Log")
![](material/log2.svg "Log")
![](material/lua.svg "Lua")

M –
![](material/markdown.svg "Markdown")
![](material/markdown_.svg "Markdown_")
![](material/markojs.svg "Markojs")
![](material/mathematica.svg "Mathematica")
![](material/matlab.svg "Matlab")
![](material/merlin.svg "Merlin")
![](material/movie.svg "Movie")
![](material/music.svg "Music")
![](material/mxml.svg "Mxml")
_._
N –
![](material/nim.svg "Nim")
![](material/node.svg "Node")
![](material/node2.svg "Node")
![](material/nodejs.svg "Node JS")
![](material/nodejs2.svg "Node JS 2")
![](material/nunjucks.svg "Nunjucks")
_._
O –
![](material/ocaml.svg "Ocaml")

P –
![](material/pdf.svg "Pdf")
![](material/perl.svg "Perl")
![](material/php.svg "Php")
![](material/powerpoint.svg "Powerpoint")
![](material/powershell.svg "Powershell")
![](material/protractor.svg "Protractor")
![](material/pug.svg "Pug")
![](material/puppet.svg "Puppet")
![](material/purescript.svg "Purescript")
![](material/python.svg "Python")
_._
R –
![](material/r.svg "R")
![](material/raml.svg "Raml")
![](material/razor.svg "Razor")
![](material/react.svg "React")
![](material/react-y.svg "React Y")
![](material/readme.svg "Readme")
![](material/reason.svg "Reason")
![](material/riot.svg "Riot")
![](material/robot.svg "Robot")
![](material/ruby.svg "Ruby")
![](material/rust.svg "Rust")

S –
![](material/sass-b.svg "SASS")
![](material/sass.svg "SASS")
![](material/scala.svg "Scala")
![](material/settings.svg "Settings")
![](material/smarty.svg "Smarty")
![](material/solidity.svg "Solidity")
![](material/sql.svg "SQL")
![](material/stylus.svg "Stylus")
![](material/swc.svg "Swc")
![](material/swift.svg "Swift")
_._
T –
![](material/table.svg "Table")
![](material/table2.svg "Table 2")
![](material/table3.svg "Table 3")
![](material/table-xl.svg "Table X")
![](material/terraform.svg "Terraform")
![](material/test-js.svg "Test Js")
![](material/test-jsx.svg "Test Jsx")
![](material/test-ts.svg "Test Ts")
![](material/tex.svg "Tex")
![](material/travis.svg "Travis - TODO: need to be fully recomposed ")
![](material/tune.svg "Tune")
![](material/twig.svg "Twig")
![](material/typescript.svg "Typescript")
![](material/typescript-def.svg "Typescript Def")

U –
![](material/unity.svg "Unity")
![](material/url.svg "Url")
_._
V –
![](material/verilog.svg "Verilog")
![](material/vfl.svg "Vfl")
![](material/virtual.svg "Virtual")
![](material/vs-o.svg "Visualstudio Outline")
![](material/vs.svg "Visualstudio")
![](material/vue-hexagon.svg "Vue Hexagon")
![](material/vue.svg "Vue")
_._
W –
![](material/watchman.svg "Watchman")
![](material/webpack.svg "Webpack")
![](material/wolframlanguage.svg "Wolframlanguage")
![](material/word.svg "Word")
_._
X –
![](material/xaml.svg "Xaml")
![](material/xml.svg "Xml")

Y –
![](material/yaml.svg "Yaml TODO: new icon needed")
![](material/yang-g.svg "Yang")
![](material/yang.svg "Yang")
![](material/yarn-s.svg "Yarn Simplified")
![](material/yarn.svg "Yarn")
_._
Z –
![](material/zip.svg "Zip")

# Minimal Icons


![](minimal/Document_16x.svg "Document_16x")
![](minimal/Document_16x_inverse.svg "Document_16x_inverse")
_._
![](minimal/Folder_16x.svg "Folder_16x") ![](minimal/Folder_16x_inverse.svg "Folder_16x_inverse")
_._
![](minimal/FolderOpen_16x.svg "FolderOpen_16x")
![](minimal/FolderOpen_16x_inverse.svg "FolderOpen_16x_inverse")

<style type="text/css">
	p {font-size:16px;line-height:16px;font-weight:700;color:#555}
	img {width: 16px;vertical-align:bottom;}
	th,td {height:16px;line-height: 16px}
	p>em {display:inline-block;width:calc(2*16px);heitht:16px;color:transparent}
</style>